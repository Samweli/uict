<?php

   class ProjectController extends Controller{
       protected $project;
       public $loader;

       public function __construct(){
           $this->loader = new Loader();
           $this->loader->model('project.php');
           $this->project = new Project();
       }

   	   public function index(){
          $community_project = $this->project->get_all();
          try{
   	   	     $this->loader->view('projects.php',$community_project);
          }catch(Exception $e){
             echo "Message: ".$e->getMessage();
          }
   	   }

       public function community_project($id=""){
          if(!empty($id)){
              try{
                 $community_project = $this->project->get_project($id);
                 $this->loader->model('comment');
                 $comment = new Comment();
                 $comment->source_id = $id;
                 $comment->category = "project";
                 $comments = $comment->get_comments();
                 $data = array(
                            'community_project'=>$community_project,
                            'comments'=>$comments
                       );
                 $this->loader->view('community-project',$data);
              }catch(Exception $e){
                 echo "Message: ".$e->getMessage();
              }
          }
       }

       public function add_project(){
            global $db;
            $this->project->title = $db->db_escape_values($_POST['title']);
            $this->project->description = $db->db_escape_values($_POST['description']);
            $this->project->begin_date = $db->db_escape_values($_POST['begin_date']);
            $this->project->initiator_id = $db->db_escape_values($_POST['initiator_id']);
            $this->project->category_id = 1;
            if($this->project->add_project()){
               redirect(URL.'user/all_projects');
            }else{
              echo "Error occured";
            }
       }
       public function search(){
	 if(isset($_GET['project_title']) && $_GET['project_title']!=NULL){
	    
	    $projects  = $this->project->get_projects_by_title($_GET['project_title']);
	    try{
   	       $this->loader->view('projects.php',$projects);
              }catch(Exception $e){
               echo "Message: ".$e->getMessage();
            }
	    
	 }
	 
	 else if(isset($_GET['begin_date']) && isset($_GET['end_date'])
	    && $_GET['begin_date'] != NULL &&  $_GET['end_date'] != NULL ){
	      $projects  = $this->project->get_by_date($_GET['begin_date'],$_GET['end_date']);
	      
	    try{
   	       $this->loader->view('projects.php',$projects);
              }catch(Exception $e){
               echo "Message: ".$e->getMessage();
            }
	 }
	 else{
	    if(isset($_GET['project_title'])){
	    header('Location:'.URL.'project/index?project_title='.$_GET['project_title'].'');
	    }else if (isset($_GET['begin_date']) ||isset($_GET['end_date'])){
	       if(isset($_GET['begin_date']) && isset($_GET['end_date'])){
		  header('Location:'.URL.'project/index?begin_date='.$_GET['begin_date'].'
			 &end_date='.$_GET['end_date'].''); 
	       }else if(isset($_GET['begin_date'])){
		 header('Location:'.URL.'project/index?begin_date='.$_GET['begin_date'].''); 
	       }
	       else if(isset($_GET['end_date'])){
		 header('Location:'.URL.'project/index?end_date='.$_GET['end_date'].''); 
	       }
	       
	    }else{
	        header('Location:'.URL.'project/index');
	    }
	 exit();
	 }
	 
       }
	 
       
       public function join_project($id=""){
        $user = (new User)->get_user($_SESSION['id']);
        
       }
   }
?>

