<?php

class ImageService{
    
    public $file_path = './pub/img/userImages/';
    
    public static $image_error;
    
    public function __construct(){
        
    }
    
    public function saveImage($name="",$type=""){
        
        if(!file_exists($name)){
            if($_FILES['file']['size'] < 2085760){
                if($_FILES['file']['size'] < 985760){
                   
                 if(move_uploaded_file($_FILES["file"]["tmp_name"],$name)){
                    return true;
                 }else{
                                 $this::$image_error = "cant move it";
                                 return false;
                    
                 }
                }else{
                     //compressing image
                     $thumb = $this->compress($_FILES["file"]["tmp_name"],$type);
                     
                     
                   if(move_uploaded_file($_FILES["file"]["tmp_name"],$name)){
                    $thumb->destroy();
                    return true;
                 } 
                }
            }else{
                $this::$image_error = "File has exceed maximum size(2MB)";
                return false;
            }
        }
        else{
             $this::$image_error = "already exist";
            return false;
        }
        
    }
    public function save_image($name=""){
        if(isset($name) && $name != NULL){
            if(!file_exists($name)){
             if(move_uploaded_file($_FILES['file']['tmp_name'], $name)){
                return true;
             }
            }else{
                return false;
            }
        }
    }
    
    public function saveFile($name="",$type=""){
        
        if(!file_exists($name)){
            if($_FILES['fileAttached']['size'] < 2085760){
                if($_FILES['fileAttached']['size'] < 985760){
                   
                 if(move_uploaded_file($_FILES["fileAttached"]["tmp_name"],$name)){
                    return true;
                 }
                }else{
                     //compressing image
                     $thumb = $this->compress($_FILES["fileAttached"]["tmp_name"],$type);
                     
                     
                   if(move_uploaded_file($_FILES["fileAttached"]["tmp_name"],$name)){
                    $thumb->destroy();
                    return true;
                 } 
                }
            }else{
                $this::$image_error = "File has exceed maximum size(2MB)";
                return false;
            }
        }
        else{
            return false;
        }
        
    }
    
    
    
    
    
    
    
    private function compress($name="",$type=""){
        if($type == "profile"){
            $thumb = new Imagick($name);
                    
            $thumb->resizeImage(640,480,Imagick::FILTER_LANCZOS,1);
            $thumb->writeImage($name);
                    
        }else if($type == "post"){
            $thumb = new Imagick($name);
                    
            $thumb->resizeImage(960,720,Imagick::FILTER_LANCZOS,1);
            $thumb->writeImage($name);
        }else{
            $thumb = new Imagick($name);
                    
            $thumb->resizeImage(640,480,Imagick::FILTER_LANCZOS,1);
            $thumb->writeImage($name);
        }
        return $thumb;
        
    }
}

?>
