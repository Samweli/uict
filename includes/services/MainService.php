<?php

// this class involves function to register user 

  class MainService{
    
    public $registrationError;
    
    public function registration(){
      $loader = new Loader();
      
      $loader->model('code.php');
      $loader->model('program.php');
        
        $code = new Code();
        $program  = new Program();
        
        $user = new User();
        $imageService = new ImageService();
        
        
        //TODO add full validation
        
        if(isset($_POST['firstname'])){
          if(preg_match('/^[A-Za-z]+$/',$_POST['firstname'])){
            $user->set_first_name($_POST['firstname']);
          }else{
            $this->registrationError = "Firstname is invalid";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
         }
        }else{
          $this->registrationError = "Enter firstname";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        if(isset($_POST['lastname'])){
          if(preg_match('/^[A-Za-z]+$/',$_POST['lastname'])){
            $user->set_last_name($_POST['lastname']);
          }else{
            $this->registrationError = "Lastname is invalid";
            
            $GLOBALS['registrationError'] = $this->registrationError;
            
             return NULL;
         }
         
        }else{
            $this->registrationError = "Enter lastname ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        if(isset($_POST['reg_number'])){
          if(preg_match('/^201[0-4]-04-0[0-9]{4}$/',$_POST['reg_number'])){
             $user->set_reg_number($_POST['reg_number']);   
          }else{
            $this->registrationError = "Wrong registration number";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
          }
        }else{
            $this->registrationError = "Enter your registration number ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        
        
        if(isset($_POST['selected_course'])){
          $user->set_program_id($_POST['selected_course']);          
        }else{
            $this->registrationError = "Enter your course ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        
         if(isset($_POST['year_of_study'])){
          $user->set_year_of_study($_POST['year_of_study']);          
        }else{
            $this->registrationError = "Enter year of study ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        if(isset($_POST['gender'])){
        $user->set_gender($_POST['gender']);  
        }else{
          $this->registrationError = "Enter gender ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        if(isset($_POST['maritial_status'])){
          $user->set_status($_POST['maritial_status']);  
        }else{
          $this->registrationError = "Enter maritial status ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        if(isset($_POST['mailing_address']) && $_POST['mailing_address']!=NULL){
          if(preg_match('/^PO.BOX.[0-9]+[A-Za-z]+$/',$_POST['mailing_address'])){
            $user->set_mailing_address($_POST['mailing_address']);
          }else{
            $this->registrationError = "Incorrect mailing address, please enter it like: eg- PO.BOX.238MBEYA";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
          }
        }
        
        if(isset($_POST['email'])){
          if(preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',$_POST['email'])){
             $user->set_email_address($_POST['email']); 
          }else{
            $this->registrationError = "Wrong email, Please enter a correct email";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
          }
        }else{
           $this->registrationError = "Enter email";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        
        
        
        if(isset($_POST['phonenumber'])){
          if(preg_match('/^0[7|6][0-9]{8}$/',$_POST['phonenumber'])){
            $user->set_phone_number($_POST['phonenumber']);
          }else{
            $this->registrationError = "Invalid phonenumber";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
          }
       
        }else{
          $this->registrationError = "Enter phonenumber ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
        if(isset($_POST['skills'])){
          $user->set_skills($_POST['skills']);
        }
        if(isset($_POST['hobbies'])){
          $user->set_hobbies($_POST['hobbies']);
        }
        
        
        
        if(isset($_POST['password']) && isset($_POST['repeatedPassword'])){
          if($_POST['password'] == $_POST['repeatedPassword']){
             $user->set_password($_POST['password']);  
          }else{
            $this->registrationError = "Passwords do not match";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
          }
        }else{
           $this->registrationError = "Enter Password ";
            $GLOBALS['registrationError'] = $this->registrationError;
             return NULL;
        }
      
        
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != NULL){
           $path = pathinfo($_FILES['file']['name']);
           
           /*incase user is changing profile picture*/
           $name = $user->get_fullName().'.'.$path['extension'];
           
           if(!file_exists('./pub/img/userImages/'.$name)){
            if(!$imageService->saveImage('./pub/img/userImages/'.$name,"profile")){
              
               $user->error = "problem saving profile picture";
               $GLOBALS['registrationError']  = "problem saving profile picture";
               if(ImageService::$image_error != NULL){
                 $GLOBALS['registrationError'] .= " ".ImageService::$image_error ;
               }
               return NULL;
            }
               $user->set_profile_picture($name);
           }else{
            $name = $user->get_fullName().date("Ymd,H:i:s").'.'.$path['extension'];
            
            if(!$imageService->saveImage('./pub/img/userImages/'.$name,"profile")){
              
               $user->error = "problem saving profile picture";
               $GLOBALS['registrationError']  = "problem saving profile picture";
               if(ImageService::$image_error != NULL){
                 $GLOBALS['registrationError'] .= " ".ImageService::$image_error ;
               }
               return NULL;
            }
            $user->set_profile_picture($name);
           }
           
           
        }else{
          $user->set_profile_picture("");
        }
        if($user->get_program_id() == 1 || $user->get_program_id() == 4 || $user->get_program_id()== 5){
        $user->set_grad_year(date('Y') + (4 - $user->get_year_of_study()));
        }else{
          $user->set_grad_year(date('Y') + (3 - $user->get_year_of_study()));
        }
        
        
        /*TODO uncomment this and then add form group on registration.php
          when user activation code is required for registration
         */
        //if(isset($_POST['activationCode'])){
        //  $code = new Code();
        //  $codes = $code->get_all();
        //  $bool = false;
        //  
        //  foreach($codes as $codeFromDb){
        //    if($codeFromDb[1] == $_POST['activationCode'] ){
        //      $bool = true;
        //     
        //      break;
        //    }
        //  }
        //  
        //  if($bool){
        //    $code = $code->get_code($_POST['activationCode']);
        //    
        //     if($code->usage == "notused"){
        //        $user->set_role($code->role);
        //        $code->usage = "used";
        //        $code->update_status();
        //     }else{
        //      $this->registrationError = "Activation code has already been used";
        //       $GLOBALS['registrationError'] = $this->registrationError;
        //     return NULL;
        //     }
        //  }else{
        //    $this->registrationError = "Activation code is not valid ";
        //    $GLOBALS['registrationError'] = $this->registrationError;
        //     return NULL;
        //  }
        //}else{
        //  $this->registrationError = "Enter Activation code";
        //    $GLOBALS['registrationError'] = $this->registrationError;
        //     return NULL;
        //}
        return $user;
    }
    
    public function validate($user=""){

      
      if($queryUsers = $user->get_all()){
      
      foreach($queryUsers as $userFromDb){
        /*this if is for used when user is being edited*/
        if(!($user->get_id() == $userFromDb['id'])){
        
        if(($user->get_first_name() == $userFromDb['first_name']) && $user->get_last_name() == $userFromDb['last_name']){
           $this->registrationError = "There is already a registered user with same firstname and lastname";
            $GLOBALS['registrationError'] = $this->registrationError;
             return false;
        }
        if($user->get_reg_number() == $userFromDb['reg_number']){
          $this->registrationError = "There is already a registered user with same registration number";
            $GLOBALS['registrationError'] = $this->registrationError;
             return false;
        }
        if($user->get_mailing_address() != NULL){
        if($user->get_mailing_address() == $userFromDb['mailing_address'] ){
          $this->registrationError = "There is already a  registered user with same mailing address";
            $GLOBALS['registrationError'] = $this->registrationError;
             return false;
        }
        }
        if($user->get_email() == $userFromDb['email_address'] ){
          $this->registrationError = "There is already a registered user with same email";
            $GLOBALS['registrationError'] = $this->registrationError;
             return false;
        }
        if($user->get_phonenumber() == $userFromDb['phone_number'] ){
          $this->registrationError = "There is already a registered user with same phonenumber";
            $GLOBALS['registrationError'] = $this->registrationError;
             return false;
        }
      }
      }
      
      return true;
      }
      return false;
    }
    
    public function add_user_in_session($user=""){
      
      
            $db = new Database();
            
            $reg_number = $db->db_escape_values($_POST['reg_number']);
	    $password = $db->db_escape_values($_POST['password']);
            
            // code to immediately put user on session after signing up
            
            $member = $user->authenticate($reg_number,$password);
            
            return $member;
         
    }
    
    //load data for user
    public function loadData(){
        $loader = new Loader();
      try{
         $loader->model("story.php");
         $loader->model("project.php");
         $loader->model("message.php");
         $loader->model("post.php");
         }catch(Exception $e){
            echo 'Message: '.$e.getMessage();
         }
         $posts = (new Post())->get_posts(3);
         $_SESSION['loaded_posts'] = 3;
         
         $stories = (new Story())->get_all();
         $user = (new User())->get_user($_SESSION["user_id"]);
         $latestUsers = (new User())->get_latest_users(3);
         $latestProjects = (new Project())->get_latest_projects(2);
         $newMessages = (new Message())->newMessage($_SESSION["user_id"]);
         $data = array(
                       "user" => $user,
                       "stories" => $stories,
                       "latestUsers" =>$latestUsers,
                       "latestProjects" => $latestProjects,
                       "newMessages" => $newMessages,
                       "posts"=>$posts
                       );
         return $data;
    }
    //searching the site
    public function search($searchQuery=""){
      if($searchQuery != NULL){
        $loader = new Loader();
        try{
         $loader->model("project.php");
          $loader->model("event.php");
        }catch(Exception $e){
          echo 'Message'.$e->getMessage();
          
        }
        
        
       $project = new Project();
       $projectResults = $project->get_projects_by_title($searchQuery);
       $event = new Event();
       $eventResults = $event->get_events_by_title($searchQuery);
        
       $results = array(
                        'projects' => $projectResults,
                        'events' =>  $eventResults
                        );
       
       return $results;
      
      }
     
    }
    
    //sending mail with new  password
    public function sendNewPass($reg_number="",$email=""){
      $loader = new Loader();
      $loader->service("EmailService.php");
      $emailService = new EmailService();
      
      if($email != NULL){
        if(preg_match('/^201[0-4]-04-0[0-9]{4}$/',$reg_number)
           && preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',$email)){
          $user = (new User())->get_user_by_email($email);
          if($user !=NULL){
            if($user->get_reg_number() == $reg_number ){
            $password = $this->generatePassword();
            $user->set_password($password);
            
            if($user->updatePass($user->get_password())){
              $to = $email;
              $subject = 'Password Recovery';
              $message ='Hello<br>'.$user->get_fullName().',<br><br>
               We have received your password recovery request,<br><br>
               Your new  password is <strong>'.$password.'</strong> ,<br>
               Login using the above password and make sure you change it.<br><br>
               
               All the Best,<br>
               UICT Community<br>
               <small>Please contact us, if you are not the one who has sent us this request</small>';
              
              if($emailService->sendMail($to, $subject, $message)){
               
              return true;
              }else{
                return false;
              }
            }else{
              return false;
            }
            }else{
              return false;
            }
          }else{
            return false;
          }
        }else{
          return false;
        }
      }
      else{
          return false;
        }
      
    }
    //TODO increase strength of generation of password
    private function generatePassword(){
      $password = hash("sha256",date('l jS \of F Y h:i:s A'));
      $password = hash("sha256",$password.'new pass');
      $password = substr($password,0,7);
      return $password;
    }
    
  }
?>