 <?php
 
 class Comment {
   
        private $id;
        private $user_id;
        private $comment;
        private $post_id;
        private $deleted;
        private $date_created;
        
        public static $comment_error;
        
        public function __construct($user_id="",$post_id="",
                                   $comment=""){
            
           
            $this->user_id = $user_id;
            $this->post_id = $post_id;
            $this->comment = $comment;
            $this->deleted = false;
            $this->date_created = date("Y-m-d H:i:s");
            
            
        }
	
	public function get_id(){
	    return $this->id;
	}
	
	public function get_user_id(){
	    return $this->user_id;
	}
	public function get_post_id(){
	    return $this->post_id;
	}
	public function get_comment(){
	    return $this->comment;
	}
	public function get_date_created(){
	    return $this->date_created;
	}
	
	public function add_comment(){
            $sql = "INSERT INTO comment (user_id,post_id,comment,date_created)";
            $sql.= "VALUES('".$this->user_id."','".$this->post_id."','".$this->comment."',
	            '".$this->date_created."')";
            
            
            global $db;

            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            }else{
                 $this::$comment_error = $db->last_query;  
            }
        }
	public function get_by_post_id($post_id=""){
	    if($post_id != NULL){
	    $sql = "SELECT * FROM comment WHERE post_id = '".$post_id."' ";
          $sql.=  "ORDER BY date_created DESC ";
          global $db;
          if($results = $db->db_query($sql)){
             $result_comments = $db->db_fetch_array($results);
             $comment_objects = $this->arrayToObject($result_comments);
                return $comment_objects;
          }else{
             $this::$comment_error = $db->last_query;  
          }
        }
	}
	//getting unseen comments
        public function newComments(){
            $sql = "SELECT * FROM comment ";
            $sql .= "";
            //$sql .= "WHERE up.user_id <> '".$_SESSION['user_id']."' AND p.id <> up.post_id";
            //$sql .= "ORDER BY p.date_created DESC";
            global $db;
            if($results = $db->db_query($sql)){
             $result_comments = $db->db_fetch_array($results);
             $post_comments = $this->arrayToObject($result_comments);
             
             $commentarr = array();
             foreach($post_comments as $comment){
		if(is_object($comment) && $comment != NULL){
                $nsql = "SELECT * FROM user_comment WHERE  comment_id = '".$comment->get_id()."' ";
                $nsql .=" AND user_id = '".$_SESSION['user_id']."' LIMIT 1";
                if($result_for_comment = $db->db_query($nsql)){
                    $result_for_comment = $db->db_fetch_array($result_for_comment);
                    
                    if(count($result_for_comment) > 0){
                       continue; 
                    }else{
                        array_push($commentarr,$comment); 
                    }
                   
                    
                }else{
                    $this::$comment_error = $db->last_query;
                }
             }
                return $commentarr;
	     }
          }else{
             $this::$comment_error = $db->last_query;  
          }
        }
	
	public function update($id){
            if(isset($id) && $id != NULL){
            $sql = "INSERT INTO user_comment (user_id,comment_id,seen,date_created)";
            $sql .="VALUES ('".$_SESSION["user_id"]."','".$id."','1','".date("Y-m-d H:i:s")."')";
            
            global $db;
            if($results = $db->db_query($sql)){
                return true;
            }else{
                $this::$comment_error = $db->last_query;
              }
            }
            
        }
	//function to return object when given an array
        
        private function arrayToObject($comments_array){
	    if(count($comments_array) > 0){
            if(count($comments_array) != 1){
		$comment_objects = array();
		for($i = 0; $i <count($comments_array) ; $i++) {
		$comment = $comments_array[$i];
	       
	       $comment = new Comment($comment['user_id'],$comment['post_id'],$comment['comment']);
	       $comment->id =  $comments_array[$i]['id'];
	       $comment->date_created = $comments_array[$i]['date_created'];
	       $comment_objects[$i] = $comment;
           }
              return $comment_objects;
        }else{
           $comment= $comments_array[0];
	   
           $comment = new Comment($comment['user_id'],$comment['post_id'],$comment['comment']);
	       
           $comment->id =  $comments_array[0]['id'];
           $comment->date_created = $comments_array[0]['date_created'];
           return $comment;
        }
	    }else{
		return NULL;
	    }
        }
	  

 }	
?>