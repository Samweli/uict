<?php

class Picture {
    private $id;
    private $url;
    private $post_id;
    private $user_id;
    private $date_created;
    
    public static $picture_error;
    
    
    public function __construct($url="",$post_id="",$user_id=""){
        $this->url = $url;
        $this->post_id = $post_id;
        $this->user_id = $user_id;
        
        $this->date_created = date("Y-m-d H:i:s");
        
    }
    
    public function set_url($url=""){
        $this->url = $url;
    }
    public function set_post_id($post_id=""){
        $this->post_id = $post_id;
    }
    public function set_user_id($user_id=""){
        $this->user_id = $user_id;
    }
    
    
    public function get_id(){
        return $this->id;
    }
    public function get_url(){
        return $this->url;
    }
    public function get_post_id(){
        return $this->post_id;
    }
    public function get_user_id(){
        return $this->user_id;
    }
    
    
    
    public function add_picture(){
        $sql = "INSERT INTO picture (url,post_id,user_id,date_created) ";
            $sql.= "VALUES('".$this->url."','".$this->post_id."','".$this->user_id."','".$this->date_created."')";
            
            global $db;

            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            }else{
                 $this::$picture_error = $db->last_query;  
            }
    }
    public function edit_picture(){
            $sql = "UPDATE picture SET url = '".$this->url."',post_id = '".$this->post_id."',";
            $sql .=" user_id = '".$this->user_id."' WHERE id ='".$this->id."'";
            global $db;
            if($db->db_query($sql)){
                   return $db->db_affected_rows();
            }else{
                   $this::$picture_error = $db->last_query;
                   return false;
            }   
    }
    
    public function get_picture($id=""){
          $sql = "SELECT * FROM picture WHERE id = '".$id."'";
          global $db;
          if($results = $db->db_query($sql)){
             $result_pictures = $db->db_fetch_array($results);
             $pictures_objects = $this->arrayToObject($result_pictures);
                return $pictures_objects;
          }else{
             $this::$picture_error = $db->last_query;  
          }
    }
    public function get_by_url($url=""){
        $sql = "SELECT * FROM picture WHERE url = '".$url."' LIMIT 1";
          global $db;
          if($results = $db->db_query($sql)){
             $result_pictures = $db->db_fetch_array($results);
             $pictures_objects = $this->arrayToObject($result_pictures);
                return $pictures_objects;
          }else{
             $this::$picture_error = $db->last_query;  
          }
    }
    
    private function arrayToObject($pictures_array){
        
        if(count($pictures_array) != 1){
            $picture_objects = array();
        for($i = 0; $i <count($pictures_array) ; $i++) {
           $picture = $pictures_array[$i];
           
           $picture = new Picture($picture['url'],$picture['post_id'],$picture['user_id']);
           $picture->id =  $pictures_array[$i]['id'];
           $picture->date_created = $pictures_array[$i]['date_created'];
           $picture_objects[$i] = $picture;
        }
        return $picture_objects;
        }else{
           $picture = $pictures_array[0];
           
           $picture = new Picture($picture['url'],$picture['post_id'],$picture['user_id']);
           $picture->id =  $pictures_array[0]['id'];
           $picture->date_created = $pictures_array[0]['date_created'];
           return $picture;
        }
    }
}




?>