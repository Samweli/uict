<?php

class Security{
    
    public $publicUrls;
    public $privateUrls;
    public $finincialUrls;
    public $projectManagerUrls;
    
    public $error;
    
    public function __construct(){
        session_start();
        $this->publicUrls = array('project','project/index','project/community_project',
                                  'project/search',
                                  'event','event/index','event/search','home/index','home/about',
                                  'home/login','home/registration','home/projects',
                                  'home/charity','home/sports','home/study','login/index','login/auth',
                                  'login/denied','logout/auth','user/add_new_project','user/add_new_event',
                                  'home/register','event/community_event','home/index','home/resetPassword',
                                  'home/test','home/sendPass','home/siteSearch'
                                  );
        
        $this->privateUrls = array('home/userhome','home/userProfile','home/editInfo',
                                   'home/editUser','user/all_events','forum',
                                   'user/all_projects','user/all_members','user','user/profile',
                                   'user/sendMessage','user/checkNewMessage','user/messages','user/search',
                                   'user/post','user/viewUser','user/checkNewPost','user/deleteMessage',
                                   'user/uploadPostImage','user/postComment','user/checkNewComments',
                                   'user/posts','user/morePosts','user/uploadPostFile','user/deletePost');
        
        $this->projectManagerUrls = array('project/add_project','projects/add_project');
        
        $this->finincialUrls = array('finance/add_income','finance/report','finance/add_expense','user/add_income','user/add_expense');
    }
    
    public function authorizeUrl($url){
        
       
        if(in_array($url,$this->publicUrls)){
            return true;
        }
        if(in_array($url,$this->privateUrls)){
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true){
              
                return true;
            }
            else{
                  $this->error = "You are not authorized to view the requested page";
                return false;
            }
        }
        
        if(in_array($url,$this->projectManagerUrls)){
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true){
            if(isset($_SESSION['role']) && $_SESSION['role']=="project_manager"){
                return true;
            }
            else{
                $this->error = "You need to be project manager to access requested page";
                return false;
            }
            }else{
                $this->error = "You are not authorized to view the requested ";
                return false;
            }
        }
        
        if(in_array($url,$this->finincialUrls)){
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true){
                
            if(isset($_SESSION['role']) && $_SESSION['role']=="finance_manager"){
                return true;
            }
            else{
                $this->error = "You need to be finance manager to access requested page";
                return false;
            }
            }else{
                $this->error = "You are not authorized to view the requested page";
                return false;
            }
        }
        $this->error = 'The page you are looking for is not available ';
        return false;
        
    }
    
    
}


?>