 <?php
         $userPosts = (new Post())->get_post_by_user_id($_SESSION['user_id']);
         
 ?>
 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id()?>"
	    title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?>
    </a>
    <span class="pull-right date-sm" ><b>
    <?php echo date('D jS \of F Y '); ?></b></span>
 <div class="visible-xs div_for_mobile">
	<div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_projects"
                       <?php echo 'class="list-group-item link_mobi '.(CurrentPage::$currentPage == "user_current_projects"?'active':'').'"';
                       ?>
                       >
                       <span class="glyphicon glyphicon-folder-open"></span> Projects</a>
	       </div>
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_events"
                       <?php echo 'class="list-group-item link_mobi '.(CurrentPage::$currentPage == "user_current_events"?'active':'').'"';
                       ?>>
                    <span class="glyphicon glyphicon-calendar"></span> Events </a>
                   
	       </div>
   
	  </div>
        <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_members"
                       <?php echo 'class="list-group-item link_mobi '.(CurrentPage::$currentPage == "all_members"?'active':'').'"';
                       ?>>
                    <span class="icon fr-torsos-all"></span> Members</a>
	       </div>
	       <div class="btn-group">
		 <a href="<?php echo URL.'user/posts/'.$_SESSION['user_id'] ?>"
                       <?php echo 'class="list-group-item link_mobi '.(CurrentPage::$currentPage == "userposts"?'active':'').'"';
                       ?>>
                    <span class="glyphicon glyphicon-comment"></span> Posts
                    <span class="post_number pull-right"><?php echo count($userPosts);?></span></a>
	       </div>
   
	  </div>
 </div>
 <div class="visible-sm">
    <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_projects"
                       <?php echo 'class="list-group-item '.(CurrentPage::$currentPage == "user_current_projects"?'active':'').'"';
                       ?>
                       >
                       <span class="glyphicon glyphicon-folder-open"></span>      Projects</a>
	       </div>
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_events"
                       <?php echo 'class="list-group-item '.(CurrentPage::$currentPage == "user_current_events"?'active':'').'"';
                       ?>>
                    <span class="glyphicon glyphicon-calendar"></span> Events </a>
                   
	       </div>
   
	  </div>
        <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <a href="<?php echo URL;?>user/all_members"
                       <?php echo 'class="list-group-item '.(CurrentPage::$currentPage == "all_members"?'active':'').'"';
                       ?>>
                    <span class="icon fr-torsos-all"></span> Members</a>
	       </div>
	       <div class="btn-group">
		 <a href="<?php echo URL.'user/posts/'.$_SESSION['user_id'] ?>"
                       <?php echo 'class="list-group-item '.(CurrentPage::$currentPage == "posts"?'active':'').'"';
                       ?>>
                    <span class="glyphicon glyphicon-comment"></span> Posts
                    <span class="post_number pull-right"><?php echo count($userPosts);?></span></a>
	       </div>
   
	  </div>
 </div>