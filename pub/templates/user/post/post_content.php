<?php
$newPosts = $data;
$output='';


if(is_array($newPosts)){
    foreach($newPosts as $post){
        $user = (new User())->get_user($post->get_user_id());
	 $comments = (new Comment())->get_by_post_id($post->get_id());
        $output .='
        <div class="post_div story_list">
             <h3 class="title">
               <span>';
               if($user->get_profile_picture() != NULL){
                 $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
               }else{
                 $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3  src="../../pub/img/avatars/profileImage.jpg" >';
               }
                $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
          if($calcDate == date("Y-m-d")){
             $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
          }else{
             $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
             '.date("H:i:s",strtotime($post->get_date_created()));
          }
          $output .= '
          </span>';
          if(!($user->get_id() == $_SESSION['user_id'])){
		    $output .= '<span class="widthper">
		               '.$user->get_fullName().' posted
		                </span>';
	    }else{
		    $output .= '<span class="widthper">
		                   Me
		                  </span>';
	    }
         $output .='
         <span class="pull-right datepost widthperDate">
          '.$displayDate.'
         </span></h3>';
         
        $output .= '
              <ul class="nav div_cont">
          <li>
          <p class="content">
          '.$post->get_content().'
          </p>
          </li>
        </ul>';
             if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		      <img class="postImage col-lg-12 col-md-12 col-sm-12 col-xs-12"  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
	       
		</div>
		</div>';
		}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments">
		       <ul  class="nav">';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			    if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			    
			   $output .='
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments">
		       <ul  class="nav">';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			     if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			   $output .='
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		$output .='
	       <div class="dropdown" id="dropdown'.$post->get_id().'" >
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display: none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
        
        $output .='
            </div>';
            (new Post())->updateSeen($post->get_id());
}
}else{
    $post = $newPosts;
        $user = (new User())->get_user($post->get_user_id());
	
	 $comments = (new Comment())->get_by_post_id($post->get_id());
        $output .='
        <div class="post_div story_list">
             <h3 class="title">
               <span>';
               if($user->get_profile_picture() != NULL){
                 $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
               }else{
                 $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
               }
                $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
          if($calcDate == date("Y-m-d")){
             $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
          }else{
             $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
             '.date("H:i:s",strtotime($post->get_date_created()));
          }
          $output .= '
          </span>';
          if(!($user->get_id() == $_SESSION['user_id'])){
		    $output .= '<span class="widthper">
		               '.$user->get_fullName().'  posted
		                </span>';
	    }else{
		    $output .= '<span class="widthper">
		                   Me
		                  </span>';
	    }
         $output .='
         <span class="pull-right datepost widthperDate">
          '.$displayDate.'
         </span></h3>';
         
        $output .= '
              <ul class="nav div_cont">
          <li>
          <p class="content">
          '.$post->get_content().'
          </p>
          </li>
        </ul>';
        if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav">
		   <li>
		      <img class="postImage col-lg-12 col-md-12 col-sm-12 col-xs-12  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
		</div>
		</div>';
	}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments">
		       <ul class="nav comment-ul" id="comm'.$post->get_id().'">';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			   
			     if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			       
			   $output .='
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments">
		       <ul class="nav comment-ul" id="comm'.$post->get_id().'">';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			      if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       } 
			   $output .= '
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		$output .='
	       <div class="dropdown">
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display:none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
		
        
        $output .='
            </div>';
            (new Post())->updateSeen($post->get_id());
	}



	echo $output;


?>