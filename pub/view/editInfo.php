<?php
/* now it is only require_once in first index.php
require_once('./includes/services/Loader.php');
*/
$loader = new Loader();
$user = $data;
try{
$loader->service('Template.php');
$loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}

$template = new Template();


// variable to detect the index page

?>

<!DOCTYPE html>
    <html lang='en'>
        <head>
            <meta charset="utf-8" />
            <title>Edit Profile</title>
	    
	         <?php
		  try{
		      $template->render('resources.php');
		  }catch(Exception $e){
		      echo 'Message'.$e->getMessage();
		  }
		   ?>
	    
            
        </head>
        <body>
	<div id="page">
       <div id="header">
       <?php
	 try{
	   $template->render('header.php');
	 }
	 catch(Exception $e){
	   echo 'Message: '. $e->getMessage();
	 }
       
       ?>
       </div>
       <div class="container">
	  <div class="row ">
   <!-- u_header -->
   <div id="bannerForRegistration">
   <div class="">
   <h2>Edit your Profile </h2>
   </div>
   </div>
   </div>
   <!-- banner -->
   <!-- header -->
   <div class="row">
    <?php
       if(isset($GLOBALS['registrationError'])){
	
         echo '<div class="registrationError">';
	  echo $GLOBALS['registrationError'];
	  echo '</div>';
	  unset($GLOBALS['registrationError']);
       }
      
       ?>
    
   
   <form action="<?php echo URL?>home/editUser" method="POST" class="form-horizontal u_row_form"
		       enctype="multipart/form-data" role="form">
   
   <div class="col-lg-6 u_row">
   <legend>Personal information:</legend>
   <div class="form-group">
   <label class="col-sm-2 control-label" >FirstName:</label>
   <div class="col-sm-6">
   <input name="firstname" type="text" class="form-control" required=""
	  <?php
	     echo 'value="'.$user->get_first_name().'"';
	    
	    ?>
	  />
   </div>
   </div>
   <div class="form-group">
   <label class="col-sm-2 control-label" >LastName:</label>
   <div class="col-sm-6">
   <input name="lastname" type="text" class="form-control" required=""
	   <?php
	     echo 'value="'.$user->get_last_name().'"';
	    ?>
	    />
   </div>
   </div>
   
   
   <div class="form-group">
   <label class="col-sm-2 control-label" >Registration NO:</label>
   <div class="col-sm-6">
   <input name="reg_number" type="text" class="form-control" required=""
	  <?php
	     echo 'value="'.$user-> get_reg_number().'"';
	    ?>
	   
	  />
   </div>
   </div>
   
   <div class="form-group">
   <label class="col-sm-2 control-label" >Degree Program</label>
   <div class="col-sm-6">
   <select name ="selected_course" required="">
   <option value="">Select Course</option>
   <option
	   <?php
             if($user->get_program_id() == 1){
	      echo 'selected="selected"';
	     }
            ?>
	   value = "1">Bsc. Computer eng.</option>
   <option
	   
	    <?php
             if($user->get_program_id() == 2){
	      echo 'selected="selected"';
	     }
            ?>
	    value = "2">Bsc. in Computer Science</option>
   <option
	    <?php
             if($user->get_program_id() == 3){
	      echo 'selected="selected"';
	     }
            ?>	   
	   value = "3">Bsc. with Computer Science</option>
   <option
	    <?php
             if($user->get_program_id() == 4){
	      echo 'selected="selected"';
	     }
            ?>	   
	   value = "4">Bsc. in Electronics and Communication</option>
   <option
	    <?php
             if($user->get_program_id() == 5){
	      echo 'selected="selected"';
	     }
            ?>	   
	   value="5">Bsc. in Telecommunication Engineering</option>
   </select>
   
   <!--<input name="program" type="text" class="form-control" />-->
   </div>
   </div>
   
   <div class="form-group">
   <label class="col-sm-2 control-label" >Year of study</label>
   <div class="col-sm-6">
   <select name ="year_of_study" required="">get_year_of_study()
   <option value="">Select year</option>
   <option
	    <?php
             if($user->get_year_of_study() == 1){
	      echo 'selected="selected"';
	     }
            ?>
	   value = "1">First year</option>
   <option
	   <?php
             if($user->get_year_of_study() == 2){
	      echo 'selected="selected"';
	     }
            ?>
	   value = "2">Second year</option>
   <option
	   <?php
             if($user->get_year_of_study() == 3){
	      echo 'selected="selected"';
	     }
            ?>
	   value = "3">Third year</option>
   <option
	   <?php
             if($user->get_year_of_study() == 4){
	      echo 'selected="selected"';
	     }
            ?>
	   
	   value = "4">Fourth year</option>
   </select>
   </div>
   </div>
   
  
   
   <div class="form-group">
   <div class="">
   <label class="col-sm-2 control-label">Sex:</label>
    <div class="btn-group col-md-4" data-toggle="buttons">
        <label class="btn u_s_button ">
            <input name="gender" type="radio" class="form-control" value="M"
	      <?php
		if($user->get_gender() == 'M'){
		  echo 'checked';
		  }
	      ?>
	    > Male
        </label>
        <label class="btn pull-right  u_s_button ">
            <input name="gender" type="radio" class="form-control" value="F"
	     <?php
	      if($user->get_gender() == 'F'){
	       echo 'checked';
	      }
	     ?>
	    > Female
        </label>
        
    </div>
    </div>
   </div>
   
 
   <div class="form-group">
    <div class="">
  <label class="col-sm-2 control-label">Marital status:</label>
    <div class="btn-group col-md-7" data-toggle="buttons">
        <label class="btn u_s_button ">
            <input name="maritial_status" type="radio" class="form-control" value="In a Relationship"
	     <?php
             if($user->get_status() == 'In a Relationship'){
	      echo 'checked';
	     }
            ?> 
	    />
	    In a Relationship
   
        </label>
        <label class="btn u_s_button">
            <input name="maritial_status" type="radio" class="form-control" value="single"
	     <?php
             if($user->get_status() == 'single'){
	      echo 'checked';
	     }
            ?> 
		   />
	    Single
   
        </label>
	<label class="btn u_s_button">
           <input name="maritial_status" type="radio" class="form-control" value="married"
	     <?php
             if($user->get_status() == 'married'){
	      echo 'checked';
	     }
            ?>   
	    />
	   Married
   
        </label>
        
    </div>
   
   
   </div>
   </div>
   
   
   
   
   <div class="u_row">
   <legend>Contacts:</legend>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Mailing Address:</label>
   <div class="col-sm-6">
   <input class="form-control" name="mailing_address" type="text" 
	  <?php
	    
	     echo 'value="'.$user->get_mailing_address().'"';
	    
	    ?>
	 
	  
	  />
   </div>
   </div>
   <div class="form-group">
   <label class="col-sm-2 control-label" >E-mail:</label>
   <div class="col-sm-6">
   <input name="email" type="text" class="form-control" required=""
        <?php
	     echo 'value="'.$user->get_email().'"';
	    
	    ?>
   />
   </div>
   </div>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Cell phone:</label>
   <div class="col-sm-6">
   <input name="phonenumber" type="text" class="form-control" required=""
            <?php
	     echo 'value="'.$user->get_phonenumber().'"';
	    ?>
   />
   </div>
   </div>
   </div>
   </div>
   <div class="u_row" >
   <div class="col-lg-6 u_row">
   <legend>Personal Profile:</legend>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Skills:</label>
   <div class="col-sm-6">
            <textarea name="skills" rows="3" cols="60" class="form-control"
	     
	    <?php
	     echo 'value="'.$user->get_skills().'"';
	    
	    ?>
	    ></textarea>
   </div>
   </div>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Hobbies & Interests:</label>
   <div class="col-sm-6">
   <textarea name="hobbies" rows="3" cols="60" class="form-control"
	     <?php
	             echo 'value="'.$user->get_hobbies().'"';
	     
	    ?> 
	     ></textarea>
   </div>
   </div>
  
   <div class="form-group">
   <label class="col-sm-2 control-label" >Change Profile Picture:</label>
   <div class="col-sm-6">
   <input type="file" class="form-control" style="padding:0px;" id="file" name="file" >
   </div>
   </div>
   
   </div>
   <div class="col-lg-6 u_row">
    <div class="u_row_for_sens">
    <span class="btn btn-primary" id="triggerPass" >Change Password</span>
   <div class="passwordDiv">
   <legend>Sensitive Details:</legend>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Password:</label>
   <div class="col-sm-6">
   <input class="form-control" name="password" type="password" />
   </div>
   </div>
   <div class="form-group">
   <label class="col-sm-2 control-label" >Repeat Password:</label>
   <div class="col-sm-6">
   <input class="form-control" name="repeatedPassword" type="password" />
   </div>
   </div>
   
   </div>
   </div>
   </div>
   
   <div class="form-group col-lg-6">
   <div class="col-sm-3">
   <input type="submit" value="Submit" class="btn u_s_button form-control u_row" />
   </div>
   </div>
   </div>
   </form>
   </div>
   </div>
	 <!--container -->
	
	
	<div class="content">
	  <?php
	  try{
	    $template->render('footer.php');
	  }
	  catch(Exception $e){
	    echo 'Message: '. $e->getMessage();
	  }
	
	?>
	</div>
	</div>
        </body>
    </html>