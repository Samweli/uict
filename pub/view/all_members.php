<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{
$loader->service('Template.php');
$loader->service('CurrentPage.php');
$members = $data['users'];
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}

CurrentPage::$currentPage = "all_members";
$template = new Template();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php
	 try{
	     $template->render('resources.php');
	 }catch(Exception $e){
	     echo 'Message'.$e->getMessage();
	 }
	  ?>
            
 <body>
<div id="page">
<div id="header">
      <?php
	try{
	  $template->render('header.php');
	}
	catch(Exception $e){
	  echo 'Message: '. $e->getMessage();
	}
      
      ?>
      </div>
<div class="container">
 <div class="row">
 <div class="col-md-3 visible-md visible-lg s_row">
     <div class="row user_photo">
      <?php
      if($data['user']->get_profile_picture() != NULL){
	  echo '<img class="img img-thumbnail" src="../pub/img/userImages/'.$data['user']->get_profile_picture().'" />';
      }else{
	      echo '<img class="img img-thumbnail" src="../pub/img/avatars/profileImage.jpg" />';
      }
      ?>
		 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id() ?>" title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?></a>
	 </div><!-- end of row for profile pictire -->
	 <div class="row user_nav">
          <?php
	    try{
	     $template->render('navigation.php',$data['posts']);
	    }catch(Exception $e){
	     echo 'Message:'.$e->getMessage();
	    }
	  ?>
	 </div><!-- end of row for info -->

</div><!-- end of col-md-3 -->
<div class="container visible-sm visible-xs s_row">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
<div class="col-md-6 s_row">
 <div class="row">
	       <div class="col-lg-12 col-md-12 ">
		  <div class="input-group">
		    <input type="text" id="searchIn" class="form-control searchIn" placeholder="Search for member">
		    <?php
		      echo '<div id="dataPage" style="display:hidden;"
		            data-value="'.CurrentPage::$currentPage.'"
			     >
			    </div>';
		     ?>
		    
		    <span class="input-group-btn">
		      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span class="glyphicon glyphicon-search"></span></button>
		    </span>
		  </div><!-- /input-group -->
		</div><!-- /.col-lg-12 -->
		<div class="col-lg-12 searchResult" id="sResult">
		  <div class="users">
		     <ul class="nav" id="resultUl">
		     
		     </ul>
		  </div>
		  
		</div>
       </div><!-- end of row for search bar -->
       <!--<div class="row visible-sm visible-xs">
	 <div class="col-sm-12 col-xs-12 ">
	 <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <button type="button" class="btn btn-default">Search</button>
	       </div>
	       
	  </div>     
       </div>
       </div>-->

     <div class="user_form">
	<!-- All Events list-->
<?php
foreach($members as $member){
    if($member["id"]!=$_SESSION["user_id"]){
     echo '<div class="content_list">';
     if($member['profile_picture']!=NULL){
       echo '<h3 class="title"><span><img src="../pub/img/userImages/'.$member['profile_picture'].'" class="img col-lg-2 col-md-2 col-sm-2 col-xs-2" title="Project Title"/></span>';
     }else{
       echo '<h3 class="title"><span><img src="../pub/img/avatars/profileImage.jpg" class="img col-lg-2 col-md-2 col-sm-2 col-xs-2" title="Project Title"/></span>';
     }
       echo $member['first_name'].' '.$member['last_name'].'</h3>';
       echo '<span class="tag">'.$member['program_id'];
   
       if($member['year_of_study'] == 1){
	    echo ' - First Year</span>';
       }else if($member['year_of_study'] == 2){
	    echo ' - Second Year</span>';
       }else if($member['year_of_study'] == 3){
	       echo ' - Third Year</span>';
       }else{
	       echo ' - Fourth Year</span>';
       }
       
       //echo '<p class="_description"></p>';
       //echo '<p class="initiator"><span class="tag">Published By </span>';
       //echo $event['first_name'].' '.$event['last_name'].'<span class="tag"> Up Coming On </span>'.$event['event_date'].'</p>'; 
       echo '<div class="dropdown">
            <ul class="nav nav-pills content_nav ">
	   <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$member["id"].'">
	     <a href="#" >
	     <span class="glyphicon glyphicon-envelope"></span> Message
	   </a>
	   
	   </li>
	  <li><a href="/user/viewUser/'.$member["id"].'"><span class="glyphicon glyphicon-comment"></span> Profile</a></li>
	   
	  
          </ul>
	  <ul class="nav" style="display:none;" id="drop'.$member["id"].'" >
	   
	     <li>
	     <div class="col-md-5 col-xs-offset-2">
	     <textarea class="message_content" name="message_content" onfocus="this.value=null;this.onfocus=null;" >
	     </textarea>
	     <br>
	     <div class="fmessage">
	       <button class="btn btn-small pull-left send"  >Send</button>
	       <span class="pull-right notification" id="notification'.$member["id"].'" style="display:none;"></span>
	     </div>
	     </div>
	 
	     </li>
	     
	   
	  </ul>
	 
	   
	   </div>
	  
	   
         
	   
       
       
    </div>';
     
    
}
}
?> 



	       </div><!-- end of row for user form -->

</div><!-- end of col-md-6 -->
<div class="col-md-3 visible-md visible-lg s_row">
            <?php
		try{
		  $template->render('left_side_menu.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
         </div><!-- end of col-md-3 -->

	       </div><!-- end u_main_content -->
		  </div>
	     </div>
      </div>
     </div>
</div>
<div class="content">
     <?php
      try{
	$template->render('footer.php');
      }
      catch(Exception $e){
	echo 'Message: '. $e->getMessage();
      }
    
    ?>
    </div>
</div>
<script type="text/javascript" src="../pub/js/jquery_min.js"></script> 
<script type="text/javascript"  src="../pub/js/bootstrap.min.js"></script>


 </body>
</html>