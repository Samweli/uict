<?php
$loader = new Loader();
if(isset($data)){
 $user = $data['user'];
 }
 try{
$loader->service('Template.php');
$loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}

CurrentPage::$currentPage = "userprofile";
$template = new Template();

?>



<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <title>Home |
UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <?php
		  try{
		      $template->render('resources.php');
		  }catch(Exception $e){
		      echo 'Message'.$e->getMessage();
		  }
	   ?>
	     
 <body>
	<div id="page"> <div id="header">
	      <?php
		try{
		  $template->render('header.php');
		} catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?> </div>
	<div class="container">
	 <div class="row u_row">

	 <div class="col-md-3 visible-md visible-lg">
	     <div class="row user_photo">
	      <?php if($data['user']->get_profile_picture() != NULL){
		  echo '<img class="img img-thumbnail"
		  src="../../pub/img/userImages/'.$data['user']->get_profile_picture().'"
		  />';
	      }else{
		      echo '<img class="img img-thumbnail"
		      src="../../pub/img/avatars/profileImage.jpg" />';
	      } ?>
			 <a href="<?php echo
			 URL.'home/userProfile/'.$data['user']->get_id() ?>"
			 title="Checkout Profile" ><?php echo
			 $_SESSION['first_name'].' '.$_SESSION['last_name'];
			 ?></a>
		 </div>
	     <!-- end of row for profile picture -->
		<!-- end of row for info -->
		<div class="row user_nav">
		   <?php
		    try{
		     $template->render('navigation.php',$data["posts"]);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
		 </div>

	 </div><!-- end of col-md-3 -->
	 <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
	 <div class="col-md-6">
	      <div class="row">
	       <div class="col-lg-12">
		  <div class="input-group">
		    <input type="text" id="searchIn" class="form-control
		    searchIn" placeholder="Search for member">
		     <?php
		      echo '<div id="dataPage" style="display:hidden;"
		            data-value="'.CurrentPage::$currentPage.'"
			     >
			    </div>';
		     ?>
		     <span
		    class="input-group-btn">
		      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span
		      class="glyphicon glyphicon-search"></span></button>
		    </span>
		  </div><!-- /input-group -->
		</div><!-- /.col-lg-12 --> <div class="col-lg-12 searchResult"
		id="sResult">
		  <div class="users">
		     <ul class="nav" id="resultUl">
		     
		     </ul>
		  </div>
		  
		</div>
       </div><!-- end of row for search bar -->
	<div class="row user_form">
	
	  
	  <?php
       if(isset($_SESSION["message"])){
	  echo ' <div class="alert-info div_for_info">';
	  echo $_SESSION["message"];
	 
	  unset($_SESSION["message"]);
	  echo ' </div>';
	   
       }
      
       ?>
	
	   <!-- All Events list-->
	       <h3>Personal info</h3>
		<ul class="nav">
		      <li class="activity_li">Registration Number: <a><?php echo
		      $_SESSION['reg_number'];?></a></li> <li
		      class="activity_li">Degree program: <a><?php echo
		      $user->get_programObj()->get_program_name();?></a></li> <li
		      class="activity_li">Email: <a><?php echo
		      $_SESSION['email_address'];?></a></li> <li
		      class="activity_li">Phone Number: <a><?php echo
		      $_SESSION['phone_number']; ?></a></li> <li
		      class="activity_li">Mailing Address: <a><?php echo
		      $user->get_mailing_address(); ?></a></li> <li
		      class="activity_li">Status <a><?php echo
		      $user->get_status(); ?></a></li> <li
		      class="activity_li">Grad Year <a><?php echo
		      $user->get_grad_year(); ?></a></li>
		      
		      
		      
		      
	       </ul> <div class="edit_button"> <?php echo '<a
	       href="'.URL.'home/editInfo/'.$user->get_id().'"
	       class="u_button">edit</a>' ?> </div>



	</div><!-- end of row for user form -->

	 </div><!-- end of col-md-6 --> <div class="col-lg-3">
	  <div class="list-group">
				<h3>Events attended</h3>
	       <ul class="nav">
				       <li class="activity_li">3rd July 2014:
				       <a>Orhanage visit</a></li> <li
				       class="activity_li">1st May 2014:
				       <a>Friendly match</a></li> <li
				       class="activity_li">22nd April 2013:
				       <a>Muhimbili Hospital Visit</a></li> <li
				       class="activity_li">More than year ago:
				       <a>Football match</a></li>
				       
				</ul> <div class="edit_button">
			 </div>
		       </div> <!-- end of col-md-3 -->

			 </div><!-- end u_main_content -->

			    </div>
		       </div>
		</div>
	       </div>
	</div> <div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		} catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?> </div>
	</div>
 </body>
