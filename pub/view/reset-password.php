<?php

/* now it is only require_once in first index.php
require_once('./includes/services/Loader.php');
*/
$loader = new Loader();


try{
   $loader->service('Template.php');
   $loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}


 

$template = new Template();

// variable to detect the index page

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Login | UICT Community</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php
		  try{
		      $template->render('resources.php');
		  }catch(Exception $e){
		      echo 'Message'.$e->getMessage();
		  }
		   ?>
</head>
<body>
  <div id="page">
	<div id="header">
          <?php
             try{
                 $template->render('header.php');
             }catch(Exception $e){
                 echo "Message: ". $e->getMessage();
             }
          ?>
    </div>

   <div class="container">
   <div class="row">
   <div class="col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 col-xs-6 col-xs-offset-2">   <!--the small uict logo was here before, it is replaced with login message now!-->
	 
	      <h3>Reseting Password</h3>
	   
      <?php
      if (strpos($data,"Login") !== false){
	echo $data;
	echo '<br><br>
	 <span><a href="'.URL.'login/index">Login Page</a></span>';
      }
      else if($data == "Login to your email to get a new password"){
	 echo '<h3>'.$data.'</h3>';
      }
      else
      {
	 if(strpos($data,"Problem") !== false){
	    echo '<h5>'.$data.'</h5>';
	 }
	 echo '
	<div class="ui_form">
       <form name="login" action="'.URL.'home/sendPass" method="post">
         <label for="reg">Enter your registration number</label>
	 <input type="text" name="reg_number" id="reg_number" required="" placeholder="Registration Number"
	 class="form-control"><br>
	 
	 <label for="email">Enter your email</label>
	 <input type="email" name="email" id="email" required="" placeholder="Email" class="form-control">
	 <br>
	 
	  <input class="u_button" type="submit" value="submit" />
	 <br><br>
	 <span><a href="'.URL.'login/index"><< Back</a></span>
       </form>
	   </div>
	   ';
      }
	 ?>
   </div>
   </div>
</div>
   
   
      <div class="content">
            <?php
                try{
                   $template->render('footer.php');
                }catch(Exception $e){
                   echo "Message: ".$e->getMessage();
                }
            ?>
            </div>
      </div>
  </div>
  
</body>
</html>
