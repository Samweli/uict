<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{
$loader->service('Template.php');
$loader->service('CurrentPage.php');
$events = $data['events'];
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}

CurrentPage::$currentPage = "user_current_events";

$template = new Template();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <?php
		  try{
		      $template->render('resources.php');
		  }catch(Exception $e){
		      echo 'Message'.$e->getMessage();
		  }
		   ?>
             
 <body>
	<div id="page">
	<div id="header">
	      <?php
		try{
		  $template->render('header.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?>
	      </div>
	<div class="container">
	 <div class="row">
         <div class="col-md-3 visible-md visible-lg s_row">
             <div class="row user_photo">
	      <?php
	      if($data['user']->get_profile_picture() != NULL){
                  echo '<img class="img img-thumbnail" src="../pub/img/userImages/'.$data['user']->get_profile_picture().'" />';
	      }else{
		      echo '<img class="img img-thumbnail" src="../pub/img/avatars/profileImage.jpg" />';
	      }
	      ?>
		 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id(); ?>" title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?></a>
		 </div><!-- end of row for profile pictire -->
		 <div class="row user_nav">
                  <?php
		    try{
		     $template->render('navigation.php',$data['posts']);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
		 </div><!-- end of row for info -->

         </div><!-- end of col-md-3 -->
	 <div class="container visible-sm visible-xs s_row">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
         <div class="col-md-6 s_row">
             <div class="row ">
	       <div class="col-lg-12 col-md-12 ">
		  <div class="input-group">
		    <input type="text" id="searchIn" class="form-control searchIn" placeholder="Search for member">
		    <?php
		      echo '<div id="dataPage" style="display:hidden;"
		            data-value="'.CurrentPage::$currentPage.'"
			     >
			    </div>';
		     ?>
		    <span class="input-group-btn">
		      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span class="glyphicon glyphicon-search"></span></button>
		    </span>
		  </div><!-- /input-group -->
		</div><!-- /.col-lg-12 -->
		<div class="col-lg-12 searchResult" id="sResult">
		  <div class="users">
		     <ul class="nav" id="resultUl">
		     
		     </ul>
		  </div>
		  
		</div>
       </div><!-- end of row for search bar -->
       <!--<div class="row visible-sm visible-xs">
	 <div class="col-sm-12 col-xs-12 ">
	 <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <button type="button" class="btn btn-default">Search</button>
	       </div>
	       
	  </div>     
       </div>
       </div>-->

			 <div class="row user_form">
			    <!-- All Events list-->

                 <?php
                    foreach($events as $event){
		      echo '<div class="content_list">';
		      echo '<div class="row">';
		      echo '<div class="col-lg-">
			<img src="../pub/img/userImages/'.$event['profile_picture'].'"
			class="img col-lg-2 col-md-2 col-sm-2 col-xs-2" title="Project Title"/>
			</div>';
			
			
			echo '<div class="col-lg-9">
			       <h3 class="title"><span>';
                        echo $event['title'].'</h3>';
                        echo '<span class="tag tag-description">Description</span>';
                        echo '<p class="_description">'.$event['description'].'</p>';
                        echo '<p class="initiator"><span class="tag">Initiated By </span>';
                        echo $event['first_name'].' '.$event['last_name'].'<span class="tag"> On </span>'.$event['event_date'].'</p>
                        </div>'; 
                        //echo '<ul class="nav nav-pills content_nav">';
                        //  echo   '<li><a href="#"><span class="glyphicon glyphicon-share"></span> Inform a friend</a></li>';
                        //  echo   '<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Comments</a></li>'; 
                        //  echo   '<li><a href="'.URL.'home/"><span class="glyphicon glyphicon-ok"></span> Join a team</a></li>';
                        //echo '</ul>';
                      echo '</div>';
		      echo '</div>';
                    }
                 ?> 


			 </div><!-- end of row for user form -->

         </div><!-- end of col-md-6 -->
         <div class="col-md-3 visible-md visible-lg s_row">
            <?php
		try{
		  $template->render('left_side_menu.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
         </div><!-- end of col-md-3 -->

			 </div><!-- end u_main_content -->
			    </div>
		       </div>
		</div>
	       </div>
	</div>
	<div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?>
	      </div>
	</div>
 </body>